<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Foo */

$this->title = 'Update Foo: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Foos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="foo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
