<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'sqlite:' . dirname(dirname(dirname(__DIR__))) . '/runtime/yii2basic_tests.sqlite',
    'charset' => 'utf8',
];
